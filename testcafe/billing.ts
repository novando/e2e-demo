import { Selector, fixture } from "testcafe"

fixture`Test MTARGET Demo App`
  .page`https://demo.mtarget.co`

test('Login', async (t) => {
  const closeAllModal = async () => {
    await t
      .click('.close-modal>*')
    const anyModalExist = await Selector('.close-modal')
    console.log('modal closed')
    if (anyModalExist !== null) await closeAllModal()
  }

  await t
    .typeText('input[name=email]', 'test@mailtarget.co')
    .typeText('input[name=password]', 'Mtarget2022?')
    .click('#button')
    .click('#mtapp>div>div>div>div>div>a') // skip without 2fa

  // all modal if exist
  let modalExist = await Selector('.close-modal')
  if (modalExist !== null) {
    await closeAllModal()
  }

  // go to billing
  const userMenuSelector = 'nav>div>ul>li:nth-child(4)'
  console.log('go to billing')
  await t
    .click(`${userMenuSelector}>*:first-child`)
    .click(`${userMenuSelector}>div>div>ul>li:nth-child(3)>*:first-child`)
    .click(`#calculator>section:nth-child(2)>div>div>div>div:nth-child(2)>div>div>div:nth-child(5)>span:nth-child(2)`)
    .click('#mtapp>div>div:nth-child(4)>div>div>div:nth-child(3)>div>button')

  
  modalExist = await Selector('.close-modal')
  if (modalExist !== null) {
    await closeAllModal()
  }
  // const userMenu = await Selector(userMenuSelector)
})
